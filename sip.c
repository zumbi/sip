/* Copyright (C) 2018 Collabora Ltd.
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation; either version 2.1 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <stdio.h>
#include <syscall.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <string.h>
#include <errno.h>
#include <err.h>
#include <sys/ioctl.h>
#include <stdlib.h>
#include <linux/fs.h>

#define EFI_FILE "/sys/firmware/efi/efivars/csr-active-config-7c436110-ab2a-4bbb-a880-fe41995c9f82"

#define CSR_ALLOW_UNTRUSTED_KEXTS	(1 << 0)
#define CSR_ALLOW_UNRESTRICTED_FS	(1 << 1)
#define CSR_ALLOW_TASK_FOR_PID		(1 << 2)
#define CSR_ALLOW_KERNEL_DEBUGGER	(1 << 3)
#define CSR_ALLOW_APPLE_INTERNAL	(1 << 4)
#define CSR_ALLOW_UNRESTRICTED_DTRACE	(1 << 5)
#define CSR_ALLOW_UNRESTRICTED_NVRAM	(1 << 6)

static struct {
	unsigned int bit;
	char *name;
} csr_bits[] = {
	{ CSR_ALLOW_UNTRUSTED_KEXTS, "untrusted_kexts" },
	{ CSR_ALLOW_UNRESTRICTED_FS, "unrestricted_fs" },
	{ CSR_ALLOW_TASK_FOR_PID, "task_for_pid" },
	{ CSR_ALLOW_KERNEL_DEBUGGER, "kernel_debugger" },
	{ CSR_ALLOW_APPLE_INTERNAL, "internal" },
	{ CSR_ALLOW_UNRESTRICTED_DTRACE, "unrestricted_dtrace" },
	{ CSR_ALLOW_UNRESTRICTED_NVRAM, "unrestricted_nvram" },
	{ 0, NULL },
};

void set_imutable_attr(char *file, int enable)
{
	int fd;
	unsigned long flags;
	fd = open (file, O_RDONLY|O_NONBLOCK);
	if (fd == -1) {
		warn("Unable to open the efivars file. Aborting.\n");
		return;
	}

	if (ioctl(fd, FS_IOC_GETFLAGS, &flags) < 0) {
		warn("Unable to read attribute flags. Trying to proceed...\n");
		close(fd);
		return;
	}

	if (enable)
		flags |= FS_IMMUTABLE_FL;
	else
		flags &= ~FS_IMMUTABLE_FL;

	if (ioctl(fd, FS_IOC_SETFLAGS, &flags) < 0)
		warn("Unable to %sable immutable flag. Trying to proceed...\n",
		     enable?"en":"dis");
	close (fd);
}

char mask_bits(char* arg)
{
	int i;
	char r = 0;
	char *e = strtok(arg, ",");

	while (e) {
		for (i = 0; csr_bits[i].name; i++)
			if (!strcmp(e, csr_bits[i].name)) {
				r |= csr_bits[i].bit;
				e = strtok(NULL, ",");
				break;
			}
		if (!csr_bits[i].name)
			err(1, "Invalid bit provided: %s\n", e);
	}
	return r;
}

void print_status(char status)
{
	if (status == 0x10)
		printf("SIP enabled\n");
	else if(status == 0x77)
		printf("SIP disabled\n");
	else
		printf("SIP custom: 0x%hhX\n", status);
}

void usage()
{
	int i;
	printf("invalid argument. "
	       "Try \"sip <status | allow | unallow> <flag>\".\n\n"
	       "<flag> is an optional comma-separated list of the following "
	       "tokens:\n");

	for (i = 0; csr_bits[i].name; i++)
		printf("\t- %s\n", csr_bits[i].name);
}

int main (int argc, char *argv[])
{
	char buffer[1024];
	int fd;
	int count;
	int o_flags;
	enum {DISALLOW, ALLOW, STATUS} cmd;

	if (argc <= 1) {
		usage();
		return 1;
	}

	if (!strcmp(argv[1], "status"))
		cmd = STATUS;
	else if (!strcmp(argv[1], "disallow"))
		cmd = DISALLOW;
	else if (!strcmp(argv[1], "allow"))
		cmd = ALLOW;
	else {
		usage();
		return 1;
	}

	if (cmd != STATUS) {
		set_imutable_attr(EFI_FILE, 0);
		o_flags = O_RDWR;
	} else
		o_flags = O_RDONLY;

	fd = open(EFI_FILE, o_flags);
	if (fd < 0) {
		err(1, "can't open variable file");
		goto err_out;
	}

	count = read(fd, buffer, 4096);
	if (count < 0) {
		err(1, "can't read from file\n");
		goto err_out;
	}

	switch (cmd) {
	case STATUS:
		print_status(buffer[4]);
		return 1;
	case ALLOW:
		if (argc < 3)
			buffer[4] = 0x77;
		else
			buffer[4] |= mask_bits(argv[2]);
		break;
	case DISALLOW:
		if (argc < 3)
			buffer[4] = 0x10;
		else
			buffer[4] &= ~mask_bits(argv[2]);
		break;
	}

	buffer[3] = 0x0;
	count = write(fd, buffer, count);
	if (count < 0) {
		err(1, "can't write to EFI variable: %s\n", strerror(errno));
		goto err_out;
	}

	set_imutable_attr(EFI_FILE, 1);
	return 0;
err_out:
	if (cmd != STATUS)
		set_imutable_attr(EFI_FILE, 1);
	return 1;
}
